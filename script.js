function ola() {
  return "olá mundo!";
}

console.log(ola());

function soma(num1, num2) {
  return num1 + num2;
}

console.log(soma(4, 5));

const imc = (a, b) => {
  return a / (b * 2);
};

console.log(imc(66, 1.83));
